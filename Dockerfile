FROM nginx:latest

LABEL maintainer="Ruzhentsev Alexandr <git@pgallery.ru>"
LABEL version="2.0"
LABEL description="Docker image Nginx for pGallery"

RUN apt-get update && apt-get install -y wget ca-certificates apt-transport-https \
    && apt-get update && apt-get -y upgrade \
    && apt-get install -y ssl-cert supervisor python-certbot-nginx procps cron git \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

COPY config/nginx.conf 			/etc/nginx/nginx.conf
COPY config/nginx-vhost.conf 		/etc/nginx/conf.d/default.conf
COPY config/nginx-vhost-ssl.conf 	/etc/nginx/conf.d/default-ssl.conf
COPY config/crontabs			/var/spool/cron/crontabs/root
COPY config/supervisord.conf 		/etc/supervisord.conf
COPY scripts/ 				/usr/local/bin/

RUN chmod 755 /usr/local/bin/letsencrypt-init \
    && chmod 755 /usr/local/bin/letsencrypt-renew \
    && chmod 755 /usr/local/bin/docker-entrypoint.sh

RUN git clone -b dev https://gitlab.com/pgallery/gallery.git /var/www/gallery

EXPOSE 80 443

CMD ["/usr/local/bin/docker-entrypoint.sh"]
